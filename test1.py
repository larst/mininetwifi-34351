#!/usr/bin/python


# *** DON'T CHANGE THE NEXT FIVE LINES
import sys
from mininet.log import setLogLevel, info
from mn_wifi.cli import CLI
from mn_wifi.net import Mininet_wifi
from mininet.link import TCLink


def topology(args):

    # Create an empty network
    net = Mininet_wifi()

    info("*** Creating access point\n")
    ap1 = net.addAccessPoint('ap1', ssid='new-ssid', mode='g', channel='1',
                             failMode="standalone", mac='00:00:00:00:00:01',
                             position='100,100,0')
    
    info('*** Creating wireless stations\n')
    net.addStation('sta1', mac='00:00:00:00:00:02', ip='10.0.0.1/8', position='70,100,0')
    net.addStation('sta2', mac='00:00:00:00:00:03', ip='10.0.0.2/8', position='130,100,0')
    

    info("*** Configuring propagation model\n")
    net.setPropagationModel(model="logDistance", exp=4.5)

    info("*** Configuring wifi nodes\n")
    net.configureWifiNodes()

    info('*** Create wired node with connection to AP\n')
    h1 = net.addHost('h1', ip='10.0.0.3/8')
    h2 = net.addHost('h2', ip='10.0.0.4/8')
    net.addLink(ap1, h1, cls = TCLink)
    net.addLink(ap1, h2, cls = TCLink)

    if '-p' not in args:
        net.plotGraph(max_x=200, max_y=200)

    info("*** Starting network\n")
    net.build()
    ap1.start([])

    info("*** Running CLI\n")
    CLI(net)

    info("*** Stopping network\n")
    net.stop()


if __name__ == '__main__':
    setLogLevel('info')
    topology(sys.argv)
